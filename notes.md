# Instalacion
  * Instalamos el SDK desde la pagina oficial de [Dart](https://dart.dev/get-dart)
    * Verificar que la ruta de la carpeta dart-sdk se encuentra en la variable __PATH__ de las variables de entorno de Windows
  * __VSCODE__
    * Instalamos el plugin de [Dart](https://marketplace.visualstudio.com/items?itemName=Dart-Code.dart-code)
    * Creamos la carpeta donde estara el proyecto
    * Desde la terminal instalamos [stagehand](https://github.com/dart-lang/stagehand) con el comando __*pub global activate stagehand*__
    * Inciamos el proyecto de Dart escribiendo el comando __*stagehand console-full*__
      * Esto nos crea un template carpetas y archivos
    * Para ejecutar el archivo lo podemos hacer con la tecla __F5__, la cual empieza el debuuging y en la pestaña debug console veremos el resultado
  * __IntelliJ__
    * En la pantalla de bievenida de IntelliJ:
      * Seleccionamos la opcion __configure__
      * En la pestaña Marketplace buscamos el plugin de __Dart__
      * Esperamos que instale y reinicie
    * Creamos el proyecto
      * En la ventana de Nuevo Proyecto seleccionamos __Dart__
      * Agregamos la ruta de donde esta el dart SDK
      * Y en este caso seleccionamos la opcion __Console Aplication__ ya que solo trabajaremos con dart puro(por asi decirlo) y no utilizaremos nada como dartweb,flutter, etc.

# S02-CL03:Hello World Dart!
* En el archivo __main.dart__:
  * Declaramos la funcion main, la cual es la funcion general, la madre de las funciones.
  * Declaramos una variable number. Mas adelante se profundizara sobre estas.
  * Hacemos una impresion en pantalla con el metodo __print__.
  * Llamamos a una funcion __printNumero(number)__ la cual declaramos fuera de la funcion main. Mas adelante se explicara mas acerca de las funciones.

# S02-CL04: Variables
* __Dart__ es un lenguaje fuertemente tipado. Pero tambien le asigna  el tipo de variable automaticamente si no la declaramos.
* Si no asignamos valor a una variable el valor por defecto es *Null*
* Para declarar una variable privada(Private caso Kotlin) se le pone un *_(guion bajo)* antes del nombre e la variable 

# S02-CL05: Final and const
* Si queremos declarar una variable en la cual su valor no va a cambiar utilizamos __final__
* La variable __const__ es muy parecido a la antarior, solo que cambia su valor en tiempo de compilacion
* En resumen:
  * __Final:__ su valor es constante, pero no depende de nadie. El valor se le asigna directamente.
  * __Const:__ su valor es constnate, pero este se asignara al compilar el programa(tiempo de compilacion) ya que depende de otros valores para obtener el suyo.
  * No se pueden mezclar variables, si usas const para asignar un valor que implica otras variables, todas las demas tendran que ser const

# S02-CL06: Numbers
* Tenemos dos tipos de numeros en Dart, los int y los double, existen mas pero la base son esos dos tipos de numeros

# S02-CL07: Strings
* Al igual que numbers todo esta explicado en el archivo strings.dart

# S02-CL08: Booleans
* Valores que guardan dos tipos de valores true o false.

# S02-CL09: Lists
* Muy parecidas a las listas de JS
* Si una lista la declaramos *var lista = const [1,2,3,4]* los valores const no pueden sere modificados

# S02-CL10: Sets
* __Sets o conjutos__ son listas desordenadas de elementos que se pueden crear en Dart
* Para declarar un Set:
  * Utilzar la palabra reservada __Set__
  * Entre mayor y menor que el tipo de datos que contendra
  * El nombre del Set
  * Los valores del Set entre *{ }*

# S02-CL11: Maps
* __Maps__ son conjunto de elementos que a diferencia de los __Sets__ permiten relacionarlos con una clave valor

# S02-CL12: Runes
* Son unos codigos de la codificacion UTF-32, nos permiten mediante strings definir iconos que podemos utlizar en nuestras apps
* [Emoji Unicode Tables](https://apps.timwhitlock.info/emoji/tables/unicode)

# S03-CL01: Funciones 
* __Dart__ es un lenguaje orientado a objetos de manera pura, esto quiere decir que el propio lenguaje tengan un tipo especifico:
  * bool, int, string, etc : el tipo de dato que devuelve la funcion
  * nombre: el nombre de la funcion
  * (parametro): parametro especificando el tipo de dato
  * return: para devolver el resultado de la funcion

# S03-CL02: Parametros opcionales
* __Parametros nombrados(named parameters)__ nos permiten poner los paramteros en el orden que nosotros queramos. EL orden en el que se ingresen los parametros no importa, ya que como su nombre lo indican, seran nombrados mediante una clave(por asi dcirlo).
* __Parametros posicionados__ tienen una posicion y en ese orden deben ser llamados.

# S03-CL03: Funciones como parametro
* Estas pueden ser:
  * Cuando se pasen como un parametro de una funcion.
  * Cuando se declara una variable y en su contenido tiene una funcion.

# S03-CL04: Funciones anonimas
* Son funciones que no tienen nombre.

# S03-CL05: Ambito lexico
* Dart es un lenguaje con un ambito lexico que es bastante profundo
* Nos podemos encontrar llamadas anidadas en las cuales cada vez que abrimos unas llaves podemos hacer uso de las variables y funciones que se encuentren dentro de estas
* Las *{}* indican un ambito
* Las variables que esten declaradas fuera de la funcion main tambien podrar ser accesibles desde cualquier nivel jerarquico del ambito lexico.

# S04-CL01: Condicionales
* Condicionales if, if else.

# S04-CL02: Bucles for
* Bucles for, forEach y for-in

# S04-CL03: Bucles while y do-while
* Bucles while y do-while

# S04-CL04: Switch y Case
* Switch y Case

# S05-CL01: Clases y Constructores
* Dart es un lenguaje orientado a objetos.

# S05-CL02: Usando constructores y atributos
* Usaremos el archivo clasesyconstructores.dart

# S05-CL03: Otros constructores
* Usaremos el archivo clasesyconstructores.dart

# S05-CL04: Metodos
* Usaremos el archivo clasesyconstructores.dart

# Final
* Seguimos el curso hasta la instalación y uso de flutter, pero los conceptos no tienen profundidad
* Se agrego el proyecto base de flutter para ver si funciona el emulador y flutter en vscode
* Hare el curso de platzi de flutter