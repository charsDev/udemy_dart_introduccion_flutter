main(){
  var sistemasOperativos = ['Windows', 'Mac', 'Ubuntu'];

  // El metodo forEach recibe como parametro una funcion
  // y es aqui donde utilizamos una funcion anonima para 
  // pasarla como parametro y no tener que hacerla por fuera
  sistemasOperativos.forEach((elemento) => print(elemento)); //debido a que solo imprime usamos =>
}

