main(){
  // Creando Map 
  var idiomas = {
    //clave: valor
    'es': 'Español',
    'en': 'Ingles',
    'fr': 'Frances'
  };

  print(idiomas['es']);

  // Metodo de Map(addAll)
  var nuevoIdioma = {'ch': 'Chino'};
  idiomas.addAll(nuevoIdioma);
  print(idiomas);

  // Creando un Map mediante una instancia de la clase Map()
  // y pasando las clave: una por una
  var alumnos = Map();
  alumnos[1] = 'Miguel';
  alumnos[2] = 'Carlos';
  alumnos[3] = 'Tavo';
  alumnos[4] = 'Luis';

  print(alumnos);
}