void main() {
  // For basico
  var animales = ['perro', 'gato', 'elefante', 'tigre'];
  for (var i = 0; i < animales.length; i++){
     print(animales[i]);
  }

  //forEach
  print('---Impresion de forEach');
  animales.forEach((animal) => print(animal));

  // for-in
  print('---Impresion de for-in');
  for (var animal in animales){
    print(animal);
  }
}