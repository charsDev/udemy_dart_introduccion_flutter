void main(){
  Persona p = Persona('Carlos', 'Garcia', 39); // Creamos una variable de tipo persona p que instancia a la clase persona y le pasamos los atributos

  p.nombre = 'Juan'; // modificamos los atributos

  print('Nombre: ${p.nombre} Apellido: ${p.apellidos} Edad: ${p.edad}'); // Imprimir los atributos en consola

  Persona p2 = Persona.sinEdad('Victor', 'Martinez');
  print('Nombre: ${p2.nombre} Apellido: ${p2.apellidos} Edad: ${p2.edad}');

  // Llamando metodo
  p.esMayor(p2);
}

class Persona { // Clase
  
  // propiedades o atibutos de la clase
  String nombre, apellidos; 
  int edad;

  // Metodo constructor, siempre lleva el nombre de la clase
  // Persona(String n, String a){
  //   this.nombre = n;
  //   this.apellidos = a;
  // }

  // Syntactic Sugar
  Persona(this.nombre, this.apellidos, this.edad);

  // Constructor alternativo: named constructors
  Persona.sinEdad(String nombre, String apellidos){
    this.nombre = nombre;
    this.apellidos = apellidos;
    this.edad = 0;
  }

  // Metodo comparar edades personas
  void esMayor(Persona p) {
    if(this.edad > p.edad){
      print('${this.nombre} es mayor que ${p.nombre}');
    } else{

      print('${this.nombre} es menor que ${p.nombre}');
    }
  }
}