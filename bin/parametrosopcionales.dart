main() {
  mostrarNombresCompletos(apellidos: 'Garcia', nombre: 'Carlos'); // no importa el orden en el que sean llamados los parametros

  mostrarInfoExtra(39, 'Villahermosa');
  mostrarInfoExtra(39);
}

// Parametros nombrados(Named Parameters)
mostrarNombresCompletos({nombre, apellidos}){ // al ser parametros nombrados deben ir entre llaves
  print('Nombre: ${nombre}, Apellidos: ${apellidos}');
}

mostrarInfoExtra(int edad, [String ciudad = 'Cancun']){ //Cuando usamos [] esos parametros son opcionales y les podemos pasar un valor por defecto para que no sea null
  print('Edad: ${edad}, Ciudad: ${ciudad}');
}