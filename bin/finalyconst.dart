main() {
  final texto = "Radio:"; // su valor no cambiara, ni afecta, ni depende de otras variables. Por eso es final

  const pi = 3.1416; // Podria ser declarada como final, pero debido a que hay otra variable que la utiliza y como esta declarada con const se requiere que esta tambien sea declarada con const

  // Calculo de radio de una circunferencia
  // radio = circunferencia / (2*pi)
  const  divisor = 2 * pi;
  const radio = 15 / divisor;

  print('${texto } ${radio.toString()}');
}