void main(){
  var alumnos = ['Miguel', 'Carlos', 'Olivia', 'Pablo'];
  bool encontrado = false;
  
  var i = 0;
  while(!encontrado){
    if(alumnos[i] == 'Pablo'){
      encontrado = true;
      print('Hemos encontrado a pablo');
    } else {
      print('El alumno ${alumnos[i]} no es Pablo. Seguimos buscando');
    }
    i++;
  }

  print('--- Do while ---');
  i = 0;
  encontrado = false;
  do {
    if(alumnos[i] == 'Olivia'){
      encontrado = true;
      print('Hemos encontrado a Olivia');
    } else {
      print('El alumno ${alumnos[i]} no es Olivia. Seguimos buscando');
    }
    i++;
  } while (!encontrado);
}