main(){
  bool esPar(int numero){
    return numero % 2 == 0;
  }
  print('¿El numero 2 es par? ${esPar(2)}');
  print('¿El numero 3 es par? ${esPar(3)}');

  esImpar(int numero) => numero % 2 != 0;
  
  print('¿El numero 3 es impar? ${esImpar(3)}');
}