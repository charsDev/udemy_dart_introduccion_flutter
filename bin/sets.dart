main(){
  // Declarando Sets
  Set<String> sistemasOperativos = {'Windows', 'MacOS', 'Ubuntu', 'CentOS'};
  Set<String> sistemasOpMoviles = {'Android', 'iOS'};

  // Ejemplo metodos de los Set
  sistemasOperativos.add('Redhat');
  print(sistemasOperativos);

  sistemasOperativos.addAll(sistemasOpMoviles);
  print(sistemasOperativos);
}