main() {
  var cadena1 = 'Hola Mundo';
  print(cadena1);
  var cadena2 = "Hola Mundo2";
  print(cadena2);
  var cadena3 = 'Yo dije: \'Hola Mundo\'';
  print(cadena3);

  //Concatenar strings
  var string1 = 'Hola '
    'Mundo '
    'Concatenado';
  print(string1);

  var string2 = 'Hola';
  var string3 = ' mundo';
  print(string2 + string3);

  //Template Literals(expresiones en strings)
  var edad = 30;
  String frase = 'Mi edad es ${edad}';
  print(frase);

  print('Mi saludo inical: ${string2 + string3}');
  print('2 + 2 = ${2+2}');
}