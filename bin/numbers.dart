main() {
  //int 
  int x = 1; // <-- Utilizamos tipo estricto al declarar variable entera, pero tambien podria ser var x = 1 y Dart reconoce el tipo.
  print(x);
  
  //double
  double y = 1.6;

  num z = -1;

  // Algunos metodos de los numeros
  print(z.abs()); // valor absoluto
  print(y.ceil()); // redondeo al siguiente numero entero
  print(y.floor()); // redondeo al antetiro numero entero

  double real = 1; // Convierte el valor a 1.0
  print(real);

  // String --> int
  int uno = int.parse('1');
  print(uno);

  // String --> double
  double unoPuntoUno = double.parse('1.1');
  print(unoPuntoUno);

  // Int --> String
  String cadenaUno = uno.toString();
  print(cadenaUno);

  double pi = 3.1416;
  String cadenaPi = pi.toStringAsFixed(2);
  print(cadenaPi);

}