main() {
  // Runes
  var coche = '\u{1F697}';
  print(coche);

  // Instanciamos la el Runes iconos a un nuevo objeto Runes que recibe varios iconos
  Runes iconos = new Runes('\u{1F6A2} \u{1F6A4} \u{1F695}');

  // print(iconos); <-- esto imprime el UTF
  
  print(new String.fromCharCodes(iconos)); // Lo hacemos un string con el metodo correspondiente para poder imprimir los iconos en pantalla
}