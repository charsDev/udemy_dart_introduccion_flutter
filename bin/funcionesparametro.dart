main(){
  // Funciones como parametro
  var listaNumeros = [1,2,3,4,5];
  listaNumeros.forEach(imprimir);

  // Funcion declarada en variable
  var pasarMayusculas = (mensaje) => mensaje.toUpperCase();
  print(pasarMayusculas('hola mundo'));

}

void imprimir(int numero){
  print(numero.toString());
}